from selenium import webdriver
#from selenium.webdriver.chrome.service import Service as ChromeService
#from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

driver = webdriver.Chrome()
#driver = webdriver.Chrome(service=ChromeService(
#    ChromeDriverManager().install()))
driver.get("https://www.google.com/")

search_box = driver.find_element_by_name("q")
search_box.send_keys("TESLA stock price")
search_box.submit()

try:
    main = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID, "main"))
    )
    stocks = main.find_elements_by_class_name("JheGif nDgy9d")
    price = stocks[0].text
    print(price)
finally:
    driver.quit()
